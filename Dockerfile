FROM ubuntu:16.04
MAINTAINER bessonov48 <bessonov48@gmail.com>

ARG DEBIAN_FRONTEND=noninteractive

ENV APACHE_RUN_USER www-data \
	APACHE_RUN_GROUP www-data \
	APACHE_LOG_DIR /var/log/apache2
	
RUN apt-get update && apt-get -y upgrade && apt-get install -y mysql-server mysql-client php-mysql libapache2-mod-php curl php-cli php-mbstring git unzip && apt-get -y install libmysqlclient-dev php iptables php-imagick wget telnet mc phpunit php-fpm php-mcrypt php-mysql php-gd php-dev php-xdebug php-curl php-intl php-zip php-mbstring php-soap apache2 && \
	a2enmod rewrite && service apache2 restart && \ 
	chmod -R 777 /var/www/ && chown -R www-data:www-data /var/www/ && \
	apt-get autoremove && apt-get clean -y && apt-get autoclean -y && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
	
ADD configs_yii2_common-local.php /var/www/configs_yii2_common-local.php
ADD configs_yii2_console-local.php /var/www/configs_yii2_console-local.php
ADD configs_yii2_params-local.php /var/www/configs_yii2_params-local.php
ADD configs_yii2_web-local.php /var/www/configs_yii2_web-local.php
ADD mysql_dump_dev_ps.sql /var/www/mysql_dump_dev_ps.sql

ADD mysql_configs_custom.cnf /etc/mysql/conf.d/mysql_configs_custom.cnf
ADD apache_configs_yii2app.conf /etc/apache2/sites-available/apache_configs_yii2app.conf
ADD ./startup.sh /opt/startup.sh
RUN a2ensite apache_configs_yii2app.conf && \
	curl -sS https://getcomposer.org/installer -o composer-setup.php && \ 
	php composer-setup.php --install-dir=/usr/local/bin --filename=composer
	
EXPOSE 8080 

ENTRYPOINT bash -C '/opt/startup.sh';'bash'

 


